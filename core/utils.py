import math
import pickle

# import keras.callbacks
# import wandb
from scipy.signal import argrelmax, savgol_filter
from tqdm import tqdm
import numpy as np
import tensorflow as tf
import keras.utils as kr_utils
from joblib import Parallel, delayed
import os
import av
from PIL import Image, ImageOps

def set_seed(seed=1):
    kr_utils.set_random_seed(seed)
    # tf.config.experimental.enable_op_determinism()


def set_gpu_growth():
    gpus = tf.config.list_physical_devices('GPU')
    if gpus:
        # Restrict TensorFlow to only use the first GPU
        try:
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
            n_gpus = len(gpus)
        except RuntimeError as e:
            # Visible devices must be set before GPUs have been initialized
            n_gpus = 0
            print(e)
    else:
        n_gpus = 0

    return n_gpus


def set_mixed_precision(mixed_precision=True):
    if mixed_precision:
        print('Mixed precision mixed_bfloat16 training')
        tf.keras.mixed_precision.set_global_policy('mixed_bfloat16')
    else:
        print('Float32 training')
        tf.keras.mixed_precision.set_global_policy('float32')


def resize_dim(w, h, target):
    """
    resize (w, h), such that the smaller side is target, keep the aspect ratio
    :return: new_w, new_h
    """
    if w >= h:
        return int(target * w / h), int(target)
    else:
        return int(target), int(target * h / w)


def extract_save_frames(vid_id, root_path, save_path, image_size=320):
    video_path = os.path.join(root_path, vid_id + '.mp4')
    if not os.path.isfile(video_path):
        #print('File not found : {}'.format(video_path))
        return None
    if not os.path.isdir(save_path):
        os.makedirs(save_path, exist_ok=True)
    
    try:
        container = av.open(video_path)
        stream = container.streams.video[0]
        vid_height = stream.height
        vid_width = stream.width

        pad_w = (max(vid_width, vid_height) - vid_width) // 2
        pad_h = (max(vid_width, vid_height) - vid_height) // 2
        padding = (pad_w, pad_h, pad_w, pad_h)
        new_w, new_h = resize_dim(vid_width + pad_w * 2, vid_height + pad_h * 2, image_size)

        frame_rate = float(stream.average_rate)

        list_frame_idx = []
        for frame in container.decode(video=0):
            frame_idx = int(float(frame.pts * stream.time_base) * frame_rate)
            if len(list_frame_idx) > 0 and frame_idx <= list_frame_idx[-1]:
                frame_idx = list_frame_idx[-1] + 1

            frame_pil = ImageOps.expand(frame.to_image(), padding).resize(size=(new_w, new_h))

            frame_pil.save(os.path.join(save_path, '{:05d}.jpg'.format(frame_idx)))

            list_frame_idx.append(frame_idx)

        empty_img = Image.new('RGB', (image_size, image_size), (0, 0, 0))
        for idx in range(list_frame_idx[-1]):
            if idx not in list_frame_idx:
                empty_img.save(os.path.join(save_path, '{:05d}.jpg'.format(idx)))

        vid_info = {'vid_id': vid_id, 'fps': frame_rate, 'n_frame': list_frame_idx[-1],
                    'dur': list_frame_idx[-1] / frame_rate}

        container.close()
    except:
        with open('error.txt', 'a') as f:
            f.write(video_path + '\n')
        return None
    return vid_info


def video_to_frames(split='train', image_size=320, min_change_duration=0.3, root_path='./'):
    if split != 'test':
        with open(f'{root_path}export/k400_mr345_{split}_min_change_duration{min_change_duration}.pkl', 'rb') as f:
            dict_data = pickle.load(f, encoding='latin1')
        list_video = list(dict_data.keys())
    else:
        with open('/mnt/Work/Dataset/LOVEU_22/gebd/Kinetics_GEBD_Test_v2.txt', 'r') as fd:  #with open(f'{root_path}export/Kinetics_GEBD_Test_v2.txt', 'r') as fd:
            list_video = [x.split(',')[0] for x in fd.readlines()]
        dict_data = None

    #data_root_path = f'/mnt/SharedProject/Dataset/LOVEU_22/gebd/{split}'
    #save_path = f'/mnt/SharedProject/Dataset/LOVEU_22/gebd/frames/{split}'
    
    data_root_path = f'/mnt/Work/Dataset/LOVEU_22/gebd/{split}'
    save_path = f'/mnt/Work/Dataset/LOVEU_22/gebd/frames/{split}'
    
    #vid_id = list_video[100]
    #extract_save_frames(vid_id, data_root_path, f'{save_path}/{vid_id}', image_size)
    
    ret = Parallel(n_jobs=16)(
        delayed(extract_save_frames)(vid_id, data_root_path, f'{save_path}/{vid_id}', image_size) for vid_id in
        tqdm(list_video))

    ret_dict = dict()
    for vid in ret:
        if vid is not None:
            ret_dict[vid['vid_id']] = vid
            if dict_data is not None:
                ret_dict[vid['vid_id']] = dict_data[vid['vid_id']]
            else:
                ret_dict[vid['vid_id']] = dict()

            ret_dict[vid['vid_id']].update(
                {'new_fps': vid['fps'], 'new_n_frame': vid['n_frame'], 'new_dur': vid['dur']})

    with open(f'{root_path}export/{split}_data.pkl', 'wb') as f:
        pickle.dump(ret_dict, f, protocol=pickle.HIGHEST_PROTOCOL)
        
def generate_frameidx_from_raw(min_change_duration=0.3, split='valnew', root_path='./'):
    """
    Adapted from https://github.com/StanLei52/GEBD/issues/3#issuecomment-1129646628
    :param min_change_duration:
    :param split:
    :return:
    """
    assert split in ['train', 'val', 'valnew', 'test']

    with open('{}export/k400_{}_raw_annotation.pkl'.format(root_path, split), 'rb') as f:
        dict_raw = pickle.load(f, encoding='lartin1')

    mr345 = {}
    for filename in dict_raw.keys():
        ann_of_this_file = dict_raw[filename]['substages_timestamps']
        if not (len(ann_of_this_file) >= 3):
            # print(f'{filename} less than 3 annotations.')
            continue

        try:
            fps = dict_raw[filename]['fps']
            num_frames = int(dict_raw[filename]['num_frames'])
            video_duration = dict_raw[filename]['video_duration']
            avg_f1 = dict_raw[filename]['f1_consis_avg']
        except:
            # print(f'{filename} exception!')
            continue

        # this is avg f1 from halo but computed using the annotation after post-processing like merge two close changes
        mr345[filename] = {}
        mr345[filename]['num_frames'] = int(dict_raw[filename]['num_frames'])
        mr345[filename]['path_video'] = dict_raw[filename]['path_video']
        mr345[filename]['fps'] = dict_raw[filename]['fps']
        mr345[filename]['video_duration'] = dict_raw[filename]['video_duration']
        mr345[filename]['path_frame'] = dict_raw[filename]['path_video'].split('.mp4')[0]
        mr345[filename]['f1_consis'] = []
        mr345[filename]['f1_consis_avg'] = avg_f1

        mr345[filename]['substages_myframeidx'] = []
        mr345[filename]['substages_timestamps'] = []
        for ann_idx in range(len(ann_of_this_file)):
            # remove changes at the beginning and end of the video;
            ann = ann_of_this_file[ann_idx]
            tmp_ann = []
            change_shot_range_start = []
            change_shot_range_end = []
            change_event = []
            change_shot_timestamp = []
            for p in ann:
                st = p['start_time']
                et = p['end_time']
                l = p['label'].split(' ')[0]
                if (st + et) / 2 < min_change_duration or (st + et) / 2 > (
                        video_duration - min_change_duration): continue
                tmp_ann.append(p)
                if l == 'EventChange':
                    change_event.append((st + et) / 2)
                elif l == 'ShotChangeGradualRange:':
                    change_shot_range_start.append(st)
                    change_shot_range_end.append(et)
                else:
                    change_shot_timestamp.append((st + et) / 2)

            # consolidate duplicated/very close timestamps
            # if two shot range overlap, merge
            sort_idices = np.argsort(change_shot_range_start)
            change_shot_range_start = np.array(change_shot_range_start)[sort_idices].tolist()
            change_shot_range_end = np.array(change_shot_range_end)[sort_idices].tolist()
            i = 0
            while i < len(change_shot_range_start) - 1:
                while change_shot_range_end[i] >= change_shot_range_start[i + 1]:
                    change_shot_range_start.remove(change_shot_range_start[i + 1])
                    if change_shot_range_end[i] <= change_shot_range_end[i + 1]:
                        change_shot_range_end.remove(change_shot_range_end[i])
                    else:
                        change_shot_range_end.remove(change_shot_range_end[i + 1])
                    if i == len(change_shot_range_start) - 1:
                        break
                i += 1

                # if change_event or change_shot_timestamp falls into range of shot range, remove this change_event
            for cg in change_event:
                for i in range(len(change_shot_range_start)):
                    if (change_shot_range_end[i] + min_change_duration) >= cg >= (
                            change_shot_range_start[i] - min_change_duration):
                        change_event.remove(cg)
                        break
            for cg in change_shot_timestamp:
                for i in range(len(change_shot_range_start)):
                    if (change_shot_range_end[i] + min_change_duration) >= cg >= (
                            change_shot_range_start[i] - min_change_duration):
                        change_shot_timestamp.remove(cg)
                        break

            # if two timestamp changes are too close,
            # remove the second one between two shot changes, two event changes; shot vs. event, remove event
            change_event.sort()
            change_shot_timestamp.sort()
            tmp_change_shot_timestamp = change_shot_timestamp
            tmp_change_event = change_event
            # """
            i = 0
            while i <= (len(change_event) - 2):
                if (change_event[i + 1] - change_event[i]) <= 2 * min_change_duration:
                    tmp_change_event.remove(change_event[i + 1])
                else:
                    i += 1
            i = 0
            while i <= (len(change_shot_timestamp) - 2):
                if (change_shot_timestamp[i + 1] - change_shot_timestamp[i]) <= 2 * min_change_duration:
                    tmp_change_shot_timestamp.remove(change_shot_timestamp[i + 1])
                else:
                    i += 1
            for i in range(len(tmp_change_shot_timestamp) - 1):
                j = 0
                while j <= (len(tmp_change_event) - 1):
                    if abs(tmp_change_shot_timestamp[i] - tmp_change_event[j]) <= 2 * min_change_duration:
                        tmp_change_event.remove(tmp_change_event[j])
                    else:
                        j += 1
            # """
            change_shot_timestamp = tmp_change_shot_timestamp
            change_event = tmp_change_event
            change_shot_range = []
            for i in range(len(change_shot_range_start)):
                change_shot_range += [(change_shot_range_start[i] + change_shot_range_end[i]) / 2]

            change_all = change_event + change_shot_timestamp + change_shot_range
            change_all.sort()
            time_change_all = change_all

            change_all = np.floor(np.array(change_all) * fps)
            tmp_change_all = []
            for cg in change_all:
                tmp_change_all += [min(num_frames - 1, cg)]

            # if len(tmp_change_all) != 0: #even after processing,
            # the list is empty/there is no GT bdy, shall still keep []
            mr345[filename]['substages_myframeidx'] += [tmp_change_all]
            mr345[filename]['substages_timestamps'] += [time_change_all]
            mr345[filename]['f1_consis'] += [dict_raw[filename]['f1_consis'][ann_idx]]

    with open(f'{root_path}export/k400_mr345_{split}_min_change_duration{min_change_duration}.pkl', 'wb') as f:
        pickle.dump(mr345, f, protocol=pickle.HIGHEST_PROTOCOL)

    print(len(mr345))

def get_bnd_signal(scores, fps, max_dur, sigma=1., score_threshold=0.1, min_threshold=0.25):
    scores_sigmoid = scores
    if fps > 5:
        score_smooth = savgol_filter(scores_sigmoid, int(fps), 3)
        order = int(fps) // 2
    else:
        score_smooth = scores_sigmoid  # gaussian_filter1d(scores_sigmoid, sigma=sigma)
        order = 2
    cur_bnd = argrelmax(score_smooth, order=order)[0]
    cur_bnd = cur_bnd[cur_bnd < math.ceil(max_dur * fps)]
    win_size = 2
    cur_bnd_cos = []

    cur_bnd = cur_bnd[scores_sigmoid[cur_bnd] > score_threshold]

    cur_bnd = (1. * cur_bnd) / fps

    filter_time = np.logical_and(cur_bnd <= max_dur - min_threshold, cur_bnd >= min_threshold)
    # cur_bnd = cur_bnd[cur_bnd < max_dur]
    cur_bnd = cur_bnd[filter_time]

    return scores_sigmoid, cur_bnd.tolist(), cur_bnd_cos


def get_boundaries(bnd_scores, video_id, video_dur, cur_fps=5):
    bnd_dict = dict()
    score_dict = dict()
    bnd_dist_dict = dict()
    for idx in range(len(video_id)):
        cur_id = video_id[idx].decode('utf-8')

        sc_sig, cur_bnd, cur_dist = get_bnd_signal(bnd_scores[idx, :, 0], cur_fps, video_dur[idx], sigma=1, score_threshold=0.1/(cur_fps/5))
        bnd_dict[cur_id] = cur_bnd
        score_dict[cur_id] = sc_sig
        bnd_dist_dict[cur_id] = cur_dist

    return bnd_dict


def challenge_eval_func(gt_path='', pred_path='', gt_dict=None, pred_dict=None, use_tqdm=False, verbose=False,
                        threshold=0.05):
    """
    https://github.com/StanLei52/GEBD/blob/main/Challenge_eval_Code/eval.py
    Latest commit 619e15c on Apr 8, 2021
    """
    # load GT files
    if gt_dict is None:
        if gt_path == '':
            gt_path = './k400_mr345_val_min_change_duration0.3.pkl'
        with open(gt_path, 'rb') as f:
            gt_dict = pickle.load(f, encoding='lartin1')

    # load output files
    if pred_dict is None:
        with open(pred_path, 'rb') as f:
            pred_dict = pickle.load(f, encoding='lartin1')

    # recall precision f1 for threshold 0.05(5%)

    tp_all = 0
    num_pos_all = 0
    num_det_all = 0

    if verbose:
        print('Evaluating F1@{}'.format(threshold))

    if use_tqdm:
        loop_list = tqdm(list(gt_dict.keys()))
    else:
        loop_list = list(gt_dict.keys())

    for vid_id in loop_list:

        # filter by avg_f1 score
        if gt_dict[vid_id]['f1_consis_avg'] < 0.3:
            continue

        if vid_id not in pred_dict.keys():
            num_pos_all += len(gt_dict[vid_id]['substages_timestamps'][0])
            continue

        # detected timestamps
        if isinstance(pred_dict[vid_id], dict):
            bdy_timestamps_det = pred_dict[vid_id]['bnd']
            pred_dict[vid_id]['dur'] = gt_dict[vid_id]['video_duration']
            pred_dict[vid_id]['fps'] = gt_dict[vid_id]['fps']
        else:
            bdy_timestamps_det = pred_dict[vid_id]

        # myfps = gt_dict[vid_id]['fps']
        my_dur = gt_dict[vid_id]['video_duration']
        ins_start = 0
        ins_end = my_dur

        # remove detected boundary outside the action instance
        tmp = []
        for det in bdy_timestamps_det:
            tmpdet = det + ins_start
            if ins_start <= tmpdet <= ins_end:
                tmp.append(tmpdet)
        bdy_timestamps_det = tmp
        if bdy_timestamps_det == []:
            num_pos_all += len(gt_dict[vid_id]['substages_timestamps'][0])
            continue
        num_det = len(bdy_timestamps_det)
        num_det_all += num_det

        # compare bdy_timestamps_det vs. each rater's annotation, pick the one leading the best f1 score
        bdy_timestamps_list_gt_allraters = gt_dict[vid_id]['substages_timestamps']
        f1_tmplist = np.zeros(len(bdy_timestamps_list_gt_allraters))
        tp_tmplist = np.zeros(len(bdy_timestamps_list_gt_allraters))
        num_pos_tmplist = np.zeros(len(bdy_timestamps_list_gt_allraters))

        for ann_idx in range(len(bdy_timestamps_list_gt_allraters)):
            bdy_timestamps_list_gt = bdy_timestamps_list_gt_allraters[ann_idx]
            num_pos = len(bdy_timestamps_list_gt)
            tp = 0
            offset_arr = np.zeros((len(bdy_timestamps_list_gt), len(bdy_timestamps_det)))
            for ann1_idx in range(len(bdy_timestamps_list_gt)):
                for ann2_idx in range(len(bdy_timestamps_det)):
                    offset_arr[ann1_idx, ann2_idx] = abs(
                        bdy_timestamps_list_gt[ann1_idx] - bdy_timestamps_det[ann2_idx])

            # print(offset_arr-threshold * my_dur)
            for ann1_idx in range(len(bdy_timestamps_list_gt)):
                if offset_arr.shape[1] == 0:
                    break
                min_idx = np.argmin(offset_arr[ann1_idx, :])
                if offset_arr[ann1_idx, min_idx] <= threshold * my_dur:
                    tp += 1
                    offset_arr = np.delete(offset_arr, min_idx, 1)

            num_pos_tmplist[ann_idx] = num_pos
            fn = num_pos - tp
            fp = num_det - tp
            if num_pos == 0:
                rec = 1
            else:
                rec = tp / (tp + fn)
            if (tp + fp) == 0:
                prec = 0
            else:
                prec = tp / (tp + fp)
            if (rec + prec) == 0:
                f1 = 0
            else:
                f1 = 2 * rec * prec / (rec + prec)
            tp_tmplist[ann_idx] = tp
            f1_tmplist[ann_idx] = f1

        ann_best = np.argmax(f1_tmplist)
        tp_all += tp_tmplist[ann_best]
        num_pos_all += num_pos_tmplist[ann_best]

        if isinstance(pred_dict[vid_id], dict):
            pred_dict[vid_id]['gt'] = bdy_timestamps_list_gt_allraters[ann_best]

    fn_all = num_pos_all - tp_all
    fp_all = num_det_all - tp_all
    if num_pos_all == 0:
        rec = 1.
    else:
        rec = tp_all / (tp_all + fn_all)
    if (tp_all + fp_all) == 0:
        prec = 0.
    else:
        prec = tp_all / (tp_all + fp_all)
    if (rec + prec) == 0:
        f1_final = 0.
    else:
        f1_final = 2 * rec * prec / (rec + prec)

    if verbose:
        print('Precision: {}   Recall: {}'.format(prec, rec))
        print('TP: {}.   FN: {}   . FP: {}'.format(tp_all, fn_all, fp_all))
        print('Num pos: {}.   Num det: {}'.format(num_pos_all, num_det_all))

    # with open('check.pkl', 'wb') as f:
    #     pickle.dump(pred_dict, f)
    return prec, rec, f1_final


class EvalCallback(tf.keras.callbacks.Callback):
    def __init__(self, val_data=None, cur_fps=5, eval_freq=1):
        self.val_data = val_data
        self.cur_fps = cur_fps
        self.best_weights = None
        self.best_f1 = -1.
        self.eval_freq = eval_freq

    def on_epoch_end(self, epoch, logs=None):
        if self.val_data is not None and (epoch +1)% self.eval_freq == 0:
            y_pred, y_vid_id, y_vid_dur = self.model.predict(self.val_data, verbose=0)
            pred_bnd = get_boundaries(y_pred, y_vid_id, y_vid_dur, cur_fps=self.cur_fps)
            scores = challenge_eval_func(pred_dict=pred_bnd, verbose=False)
            if scores[2] > self.best_f1:
                self.best_weights = self.model.get_weights()
                self.best_f1 = scores[2]
            eval_metric = {'val-f1': scores[2], 'val-prec': scores[0], 'val-rec': scores[1]}
            logs.update(eval_metric)

    def on_train_end(self, logs=None):
        if self.best_weights is not None:
            self.model.set_weights(self.best_weights)


class LrLogger(tf.keras.callbacks.Callback):
    def __init__(self, log_freq=1, log_level='step'):
        """ Log learning rate at step/epoch every log_freq step/epoch, default on step"""
        super(LrLogger, self).__init__()
        self.log_freq = log_freq
        self.log_level = log_level

    def get_current_lr(self):
        if isinstance(self.model.optimizer.learning_rate, tf.keras.optimizers.schedules.LearningRateSchedule):
            current_lr = self.model.optimizer.learning_rate(self.model.optimizer.iterations)
        else:
            current_lr = self.model.optimizer.learning_rate

        return current_lr

    def on_batch_end(self, batch, logs=None):
        if self.log_level == 'step' and batch % self.log_freq == 0:
            current_lr = self.get_current_lr()
            logs.update({'lr': current_lr})

    def on_epoch_end(self, epoch, logs=None):
        if self.log_level == 'epoch':
            current_lr = self.get_current_lr()

            logs.update({'lr': current_lr})

    def on_train_end(self, logs=None):
        if self.log_level == 'step' and self.model.optimizer.iterations % self.log_freq != 0:
            current_lr = self.get_current_lr()
            logs.update({'lr': current_lr})


if __name__ == '__main__':
    #for thresh in np.arange(0.05, 0.5, 0.05):
        #scores = challenge_eval_func(pred_path='./submission_github.com_MCG-NJU_DDM.pkl', verbose=False,
                                     #threshold=thresh)
        #print(thresh, scores[-1])
    #pass

    #generate_frameidx_from_raw(split='train')
    #generate_frameidx_from_raw(split='val')
    #print('Train')
    #video_to_frames(split='train')
    #print('Val')
    #video_to_frames(split='val')
    print('Test')
    video_to_frames(split='test', image_size=224)
