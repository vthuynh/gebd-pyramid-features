"""
Author: Van Thong Huynh
Affiliation: Dept. of AI Convergence, Chonnam Nat'l Univ.
"""

import keras
from keras.applications import resnet, resnet_v2
import keras.layers as layers
from official.vision.modeling.layers.nn_layers import SpatialPyramidPooling
from official.projects.movinet.modeling import movinet, movinet_model, movinet_layers
import tensorflow as tf


class ResNetBackbone(keras.Model):
    def __init__(self, input_shape=(224, 224, 3), version=1, weights='imagenet', level='0', **kwargs):
        input_tensor = layers.Input(shape=input_shape)
        if version == 1:
            base = resnet.ResNet50(include_top=False, weights=weights, input_tensor=input_tensor, pooling='avg')
        elif version == 2:
            base = resnet_v2.ResNet50V2(include_top=False, weights=weights, input_tensor=input_tensor, pooling='avg', )
        else:
            raise ('ResNetBackbone do not know version {}'.format(version))

        base.trainable = True

        last_output = base.output

        outputs = dict()

        if '0' in level:
            outputs.update({'feat_last': last_output})

        if '1' in level:
            conv2_out = base.get_layer('conv2_block3_out').output
            conv2_out = layers.GlobalAvgPool2D()(conv2_out)
            outputs.update({'feat_conv2': conv2_out})
        if '2' in level:
            conv3_out = base.get_layer('conv3_block4_out').output
            conv3_out = layers.GlobalAvgPool2D()(conv3_out)
            outputs.update({'feat_conv3': conv3_out})
        if '3' in level:
            conv4_out = base.get_layer('conv4_block6_out').output
            conv4_out = layers.GlobalAvgPool2D()(conv4_out)
            outputs.update({'feat_conv4': conv4_out})

        print('Backbone features: ', outputs.keys())
        super(ResNetBackbone, self).__init__(inputs=base.inputs,
                                             outputs=outputs,
                                             **kwargs)

        self._config_dict = {'input_shape': input_shape, 'version': version, 'weights': weights, 'level': level}
        self.num_features = {'feat_last': 2048, 'feat_conv2': 256, 'feat_conv3': 512, 'feat_conv4': 1024}

    def get_config(self):
        return self._config_dict


class MoviNetBackbone(keras.Model):
    def __init__(self, input_shape=(50, 224, 224, 3), version='a2-400', level='0', weights=None, **kwargs):
        """
        MoviNet example at https://github.com/tensorflow/models/tree/master/official/projects/movinet
        :param input_shape:
        :param version:
        :param kwargs:
        """
        input_tensor = layers.Input(shape=input_shape)
        conv_type = '2plus1d'
        se_type = '2plus3d'
        activation = 'hard_swish'
        gating_activation = 'hard_sigmoid'
        pool = 'none'
        if version == 'a2-400':
            base = movinet.Movinet(model_id='a2', causal=False, use_external_states=False, output_states=False,
                                   input_specs=input_tensor, use_sync_bn=False,
                                   conv_type=conv_type,
                                   se_type=se_type,
                                   activation=activation,
                                   gating_activation=gating_activation,
                                   stochastic_depth_drop_rate=0.2,
                                   average_pooling_type=pool
                                   )
            # base = self.get_backbone_pretrained(input_tensor, model_id='a2', trainable=False)
        else:
            raise ValueError('Do not support movinet-{} at this time'.format(version))

        # last_output, _ = base.output  # get_backbone_pretrained
        last_output = base.output  # movinet.Movinet

        outputs = dict()
        if '0' in level:
            block4_out = last_output['block4_layer6']  # 7 x 7 x 144
            # block4_out = tf.reduce_mean(block4_out, axis=(-2, -3))
            # block4_out = last_output['head']  # 7 x 7 x 480  # average_pooling_type='none'
            block4_out = tf.reduce_mean(block4_out, axis=(-2, -3))
            outputs.update({'feat_last': block4_out})
        if '1' in level:
            block1_out = last_output['block0_layer2']  # 56 x 56 x 16
            # block1_out, _ = movinet_layers.Head(project_filters=80, conv_type=conv_type, activation=activation,
            #                                  average_pooling_type=pool, name='s1-head')(block1_out)
            block1_out = tf.reduce_mean(block1_out, axis=(-2, -3))
            outputs.update({'feat_conv2': block1_out})
        if '2' in level:
            block2_out = last_output['block1_layer4']  # 28 x 28 x 40
            # block2_out, _ = movinet_layers.Head(project_filters=160, conv_type=conv_type, activation=activation,
            #                                  average_pooling_type=pool, name='s2-head')(block2_out)
            block2_out = tf.reduce_mean(block2_out, axis=(-2, -3))
            outputs.update({'feat_conv3': block2_out})
        if '3' in level:
            block3_out = last_output['block3_layer5']  # 14 x 14 x 72
            # block3_out, _ = movinet_layers.Head(project_filters=320, conv_type=conv_type, activation=activation,
            #                                  average_pooling_type=pool, name='s3-head')(block3_out)
            block3_out = tf.reduce_mean(block3_out, axis=(-2, -3))
            outputs.update({'feat_conv4': block3_out})

        print('Backbone features: ', outputs.keys())
        super(MoviNetBackbone, self).__init__(inputs=base.inputs,
                                              outputs=outputs,
                                              **kwargs)

        self.num_features = {'feat_last': 144, 'feat_conv2': 16, 'feat_conv3': 40, 'feat_conv4': 72}
        # self.num_features = {'feat_last': 640, 'feat_conv2': 80, 'feat_conv3': 160, 'feat_conv4': 320}
        self._config_dict = {'input_shape': input_shape, 'version': version, 'weights': weights, 'level': level}

    @staticmethod
    def get_backbone_pretrained(input_tensor, model_id='a0', trainable=False):
        backbone = movinet.Movinet(model_id=model_id, causal=False, use_external_states=False)
        model = movinet_model.MovinetClassifier(backbone, num_classes=600, output_states=False,
                                                input_specs={'image': input_tensor})

        checkpoint_dir = '/home/hvthong/sXProject/GEBD_optimal/pretrained/movinet_{}_base/'.format(model_id)
        checkpoint_path = tf.train.latest_checkpoint(checkpoint_dir)
        checkpoint = tf.train.Checkpoint(model=model)
        status = checkpoint.restore(checkpoint_path)
        status.assert_existing_objects_matched()

        if not trainable:
            model.trainable = False
        return model.backbone
