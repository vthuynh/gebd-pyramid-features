# Generic Event Boundary Detection in Video with Pyramid Features



* To generate **tfrecord files** for using within this repo, refer to `core/dataloader.py` for [Kinetics-GEBD](https://openaccess.thecvf.com/content/ICCV2021/papers/Shou_Generic_Event_Boundary_Detection_A_Benchmark_for_Event_Segmentation_ICCV_2021_paper.pdf) and `core/tapos_utils.py#L350` for [TAPOS](https://sdolivia.github.io/TAPOS/) datasets.
* The train the best system with F!@0.05 = 0.82 on GEBD 2023 test set, run `python main.py --cfg experiments/config_seq_100.yaml`. The trained weights of this config is uploaded `experiments/config_seq_100.h5`.
