FROM tensorflow/tensorflow:2.12.0-gpu
RUN pip install tf-models-official==2.12.0 tensorflow-io scipy==1.8.1 tqdm==4.64.0 yacs==0.1.8 contextlib2==21.6.0 av wandb --no-cache-dir
